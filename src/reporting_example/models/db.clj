(ns reporting-example.models.db
  (:require [clojure.java.jdbc :as sql])
  (:import java.sql.DriverManager))

(def db {:classname   "org.sqlite.JDBC",
         :subprotocol "sqlite",
         :subname     "db.sq3"})

(defn create-employee-table []
  (sql/with-connection
    db
    (sql/create-table
      :employee
      [:name "varchar(50)"]
      [:occupation "varchar(50)"]
      [:place "varchar(50)"]
      [:country "varchar(50)"])))

(defn add-some-data []
  (sql/with-connection
    db
    (sql/insert-rows
      :employee
      ["Alebert Einshein", "Engineer", "Ulm", "Germany"]
      ["Alfred Hitchcock", "Movie Director", "London", "UK"]
      ["Wernher Von Braun", "Rocket Sientist", "Wyrzysk", "Poland"]
      ["Sigmund Freud", "Neurologist", "Pribor", "Czech Republic"]
      ["Mahatma Gandhi", "Lawyer", "Gujarat", "India"]
      ["Sachin Tenduldar", "Cricket Player", "Mumbai", "India"]
      ["Michael Schumacher", "F1 Racer", "Cologne", "Germany"])))

(defn read-employees []
  (sql/with-connection db
    (sql/with-query-results rs ["SELECT * FROM EMPLOYEE"] (doall rs))))
