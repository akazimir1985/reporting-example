(ns reporting-example.handler
  (:require [compojure.core :refer [defroutes routes]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.file-info :refer [wrap-file-info]]
            [hiccup.middleware :refer [wrap-base-url]]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [reporting-example.routes.home :refer [home-routes]]
            [reporting-example.models.db :as db]))

(defn init []
  (println "reporting-example is starting")
  (if-not (.exists (java.io.File. "./db.sq3"))
    (do
      (db/create-employee-table)
      (db/add-some-data))))

(defn destroy []
  (println "reporting-example is shutting down"))

(defroutes app-routes
  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (-> (routes home-routes app-routes)
      (handler/site)
      (wrap-base-url)))
